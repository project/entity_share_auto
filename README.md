# Entity Share Auto
This module extends the Entity Share module and help to automate the process of sharing entities.
This module use drupal queue to sync entity from server to client

## Configuration

Entity Share defines various configurations for import at `/admin/config/services/entity_share/import_config`.
This module is configured at `/admin/config/services/entity_share_auto`

Before adding any configuration firstly configure Entity Share module on both server and client.

### Client configuration

Enable Entity share auto client on client site.
Provide configuration as needed here `/admin/config/services/entity_share_auto`

### Server configuration

Enable Entity share auto server module on server site and create Channel auto configuration.
You need to create Same number of channel auto as channels in Entity share module.

## Share entity by drush
Create node on server site after run following drush command for exporting node to client side.
<br><code> drush queue:run entity_share_auto_export</code>

Now run following command on client site.
<br><code> drush queue:run entity_share_auto_import</code>
