<?php

namespace Drupal\entity_share_auto\Event;

use Drupal\Core\Entity\EntityInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class EntityShareAutoEvent definition.
 */
class EntityShareAutoEvent extends Event {

  /**
   * Entity enqueue event.
   */
  const ENTITY_SHARE_AUTO_ENTITY_ENQUEUE = 'entity_share_auto.entity.enqueue';

  /**
   * Entity import event.
   */
  const ENTITY_SHARE_AUTO_ENTITY_IMPORT = 'entity_share_auto.entity.import';

  /**
   * Node entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * Channel id.
   *
   * @var string
   */
  protected $channelId;

  /**
   * Constructs a node insertion demo event object.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity to be import.
   * @param string $channel_id
   *   Channel id.
   */
  public function __construct(EntityInterface $entity, $channel_id) {
    $this->entity = $entity;
    $this->channelId = $channel_id;
  }

  /**
   * Get the inserted entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Return entity object.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Get channel id.
   *
   * @return string
   *   Return channel id.
   */
  public function getChannelId() {
    return $this->channelId;
  }

}
