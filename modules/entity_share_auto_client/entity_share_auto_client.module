<?php

/**
 * @file
 * Contains entity_share_auto_client.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\entity_share_client\ImportContext;

/**
 * Implements hook_help().
 */
function entity_share_auto_client_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the entity_share_auto_client module.
    case 'help.page.entity_share_auto_client':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Entity share auto client module.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Import entity.
 *
 * @param array $data
 *   Item data.
 */
function entity_share_auto_client_instant_import_entity(array $data) {
  try {
    $import_context = new ImportContext($data['remote_id'], $data['channel_id'], $data['import_config_id']);
    /** @var \Drupal\entity_share_client\Service\ImportServiceInterface $import_service */
    $import_service = \Drupal::service('entity_share_client.import_service');
    $ids = $import_service->importEntities($import_context, [$data['uuid']], FALSE);
    if (!empty($ids)) {
      $logs = [
        'message' => 'Success',
        'data' => $data,
        'ids' => $ids,
      ];
    }
    else {
      $logs = [
        'message' => 'Something went wrong.',
        'data' => $data,
      ];
    }
    entity_share_auto_client_instant_server_log($logs, $data);
  }
  catch (\Exception $exception) {
    watchdog_exception('entity_share_auto_client', $exception);
    $logs = [
      'message' => 'Something went wrong.',
      'data' => $data,
    ];
    entity_share_auto_client_instant_server_log($logs, $data);
  }
}

/**
 * Send logs to server.
 *
 * @param array $logs
 *   Logs array.
 * @param array $data
 *   Item data.
 */
function entity_share_auto_client_instant_server_log(array $logs, array $data) {
  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
  $entity_type_manager = \Drupal::service('entity_type.manager');
  /** @var \Drupal\entity_share_client\Entity\Remote $remote_entity */
  $remote_entity = $entity_type_manager->getStorage('remote')->load($data['remote_id']);
  $remote_url = $remote_entity->get('url');
  $post_url = $remote_url . '/entity_share_auto_instant/updateLog';
  $config = \Drupal::service('config.factory')
    ->get('entity_share_auto.entity_share_auto_client.config');
  if ($api_key = $config->get('api_key')) {
    $post_url .= "?api_key=$api_key";
  }
  try {
    // Unpublished entity in case of failure.
    if ($logs['message'] != 'Success') {
      $entity = $entity_type_manager->getStorage('node')->loadByProperties(['uuid' => $data['uuid']]);
      if (!empty($entity)) {
        $entity = reset($entity);
        $entity->setPublished(FALSE);
        $entity->save();
      }
    }
    /** @var \GuzzleHttp\ClientInterface $http_client */
    $http_client = \Drupal::service('http_client');
    $http_client->post($post_url, [
      'json' => $logs,
    ]);
  }
  catch (\Exception $exception) {
    watchdog_exception('entity_share_auto_client', $exception);
  }

}
