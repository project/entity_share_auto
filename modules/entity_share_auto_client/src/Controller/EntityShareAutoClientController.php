<?php

namespace Drupal\entity_share_auto_client\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EntityShareAutoClientController definition.
 */
class EntityShareAutoClientController extends ControllerBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Import queue helper.
   *
   * @var \Drupal\entity_share_auto_client\Service\EntityShareAutoQueueImportHelperInterface
   */
  protected $importQueueHelper;

  /**
   * Client config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $clientConfig;

  /**
   * Current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->importQueueHelper = $container->get('entity_share_auto_client.queue_helper');
    $instance->clientConfig = $container->get('config.factory')
      ->get('entity_share_auto.entity_share_auto_client.config');
    $instance->request = $container->get('request_stack')->getCurrentRequest();
    return $instance;
  }

  /**
   * Get remote and import config.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Return .
   */
  public function getRemoteAndImportConfig() {
    try {
      // Check is client configured with API key validation or not.
      if ($api_key = $this->clientConfig->get('api_key')) {
        if ($this->request->query->get('api_key') != $api_key) {
          return new Response("Access denied.", 403);
        }
      }
      $remotes = $this->entityTypeManager->getStorage('remote')->loadMultiple();
      $import_config = $this->entityTypeManager->getStorage('import_config')
        ->loadMultiple();

      if (!empty($remotes) && !empty($import_config)) {
        $config = [];
        foreach ($remotes as $remote_id => $remote) {
          $config['remote'][$remote_id] = $remote->label();
        }
        foreach ($import_config as $import_config_id => $import) {
          $config['import_config'][$import_config_id] = $import->label();
        }

        return new Response(json_encode($config), 200);
      }
      else {
        return new Response(404, 'No remote or import config defined.');
      }
    }
    catch (\Exception $exception) {
      watchdog_exception('entity_share_auto_client', $exception);
      return new Response('Not able to process you request at the moment. Please try again later.', 500);
    }

  }

  /**
   * Add import to queue.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Return response for client.
   */
  public function addImport(Request $request) {
    if (!empty($request->getContent())) {
      // Check is client configured with API key validation or not.
      if ($api_key = $this->clientConfig->get('api_key')) {
        if ($request->query->get('api_key') != $api_key) {
          return new Response("Access denied.", 403);
        }
      }
      $data = json_decode($request->getContent(), TRUE);
      if (!empty($data['uuid']) && !empty($data['remote_id']) && !empty($data['channel_id']) && !empty($data['import_config_id'])) {
        try {
          $this->importQueueHelper->enqueue(
            $data['remote_id'],
            $data['channel_id'],
            $data['import_config_id'],
            $data['uuid']
          );
          return new Response('Success', 200);
        }
        catch (\Exception $exception) {
          watchdog_exception('entity_share_auto_client', $exception);
          return new Response('Not able to process you request at the moment. Please try again later.', 500);
        }
      }
      else {
        return new Response('Invalid data', 400);
      }
    }
    return new Response('Invalid data', 400);
  }

  /**
   * Add import to queue.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Return response for client.
   */
  public function instantImport(Request $request) {
    if (!empty($request->getContent())) {
      // Check is client configured with API key validation or not.
      if ($api_key = $this->clientConfig->get('api_key')) {
        if ($request->query->get('api_key') != $api_key) {
          return new Response("Access denied.", 403);
        }
      }
      $data = json_decode($request->getContent(), TRUE);
      if (!empty($data['uuid']) && !empty($data['remote_id']) && !empty($data['channel_id']) && !empty($data['import_config_id'])) {
        drupal_register_shutdown_function('entity_share_auto_client_instant_import_entity', $data);
        return new Response(json_encode([
          'message' => 'Import started',
          'data' => $data,
        ]), 200);
      }
      else {
        return new Response(json_encode([
          'message' => 'Invalid data',
          'data' => $data,
        ]), 400);
      }
    }
    return new Response(json_encode([
      'message' => 'Invalid data',
    ]), 400);
  }

}
