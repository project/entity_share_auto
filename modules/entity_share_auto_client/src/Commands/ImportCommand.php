<?php

namespace Drupal\entity_share_auto_client\Commands;

use Drupal\entity_share_auto_client\Service\EntityShareImportQueue;
use Drush\Commands\DrushCommands;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * A Drush commandfile.
 */
class ImportCommand extends DrushCommands {


  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * Drupal\entity_share_auto_client\Service\EntityShareImportQueue definition.
   *
   * @var \Drupal\entity_share_auto_client\Service\EntityShareImportQueue
   */
  protected $entityShareImportQueue;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityShareImportQueue $entityShareImportQueue, LoggerChannelFactoryInterface $loggerFactory) {
    parent::__construct();
    $this->loggerFactory = $loggerFactory;
    $this->entityShareImportQueue = $entityShareImportQueue;
  }

  /**
   * Import entities.
   *
   * @command import:shared-entities
   * @aliases imse
   *
   * @usage import:shared-entities
   */
  public function importEntities() {
    try {
      $this->entityShareImportQueue->process();
      $batch =& batch_get();
      if ($batch) {
        drush_backend_batch_process();
      }
      $this->io()->writeln('Import run successfully.');
    }
    catch (\Exception $exception) {
      $this->io()->error('There is an error importing entities.');
      watchdog_exception('entity_share_auto_client', $exception);
    }

  }

}
