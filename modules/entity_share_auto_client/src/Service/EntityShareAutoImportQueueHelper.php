<?php

namespace Drupal\entity_share_auto_client\Service;

use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\State\StateInterface;

/**
 * Populate a queue for client.
 *
 * @package Drupal\entity_share_auto_client\Service
 */
class EntityShareAutoImportQueueHelper implements EntityShareAutoQueueImportHelperInterface {

  /**
   * The queue factory service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * QueueHelper constructor.
   *
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(
    QueueFactory $queue_factory,
    StateInterface $state
  ) {
    $this->queueFactory = $queue_factory;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public function enqueue($remote_id, $channel_id, $import_config_id, $uuid) {
    $queue = $this->queueFactory->get(EntityShareAutoQueueImportHelperInterface::QUEUE_NAME);

    $import_states = $this->state->get(EntityShareAutoQueueImportHelperInterface::STATE_ID, []);
    $import_states_current = $import_states[$remote_id][$channel_id][$import_config_id] ?? [];
    if (empty($import_states) || !in_array($uuid, $import_states_current)) {
      // Add the entity to the export states.
      $import_states[$remote_id][$channel_id][$import_config_id][] = $uuid;

      // Create queue item.
      $item = [
        'uuid' => $uuid,
        'remote_id' => $remote_id,
        'channel_id' => $channel_id,
        'import_config_id' => $import_config_id,
      ];

      $queue->createItem($item);
    }

    // Update states.
    $this->state->set(EntityShareAutoQueueImportHelperInterface::STATE_ID, $import_states);
  }

}
