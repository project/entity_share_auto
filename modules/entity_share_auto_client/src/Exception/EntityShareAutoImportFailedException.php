<?php

namespace Drupal\entity_share_auto_client\Exception;

/**
 * Class EntityShareAutoImportFailedException definition.
 */
class EntityShareAutoImportFailedException extends \Exception {

}
