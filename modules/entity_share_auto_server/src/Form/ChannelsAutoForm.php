<?php

namespace Drupal\entity_share_auto_server\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ChannelsAutoForm definition.
 */
class ChannelsAutoForm extends EntityForm {

  /**
   * Guzzle\Client instance.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a ChannelForm object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   Http client.
   */
  public function __construct(
    ClientInterface $http_client
  ) {
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $channels_auto = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $channels_auto->label(),
      '#description' => $this->t("Label for the Channels Auto."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $channels_auto->id(),
      '#machine_name' => [
        'exists' => '\Drupal\entity_share_auto_server\Entity\ChannelsAuto::load',
      ],
      '#disabled' => !$channels_auto->isNew(),
    ];

    $form['channel_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Channel type'),
      '#default_value' => $channels_auto->get('channel_type'),
      '#options' => $this->getChannelsEntityOptions(),
      '#empty_value' => '',
      '#required' => TRUE,
    ];

    $form['remote_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Remote URL'),
      '#maxlength' => 255,
      '#description' => $this->t("Add remote url to fetch remote and import config. Make sure you have enabled 'Entity share auto client' module on remote site. Don't add / at the end of URL."),
      '#required' => TRUE,
      '#default_value' => $channels_auto->get('remote_url'),
      '#ajax' => [
        'disable-refocus' => FALSE,
        'callback' => [get_class($this), 'buildAjaxRemoteSelect'],
        'effect' => 'fade',
        'event' => 'change',
        'method' => 'replace',
        'wrapper' => 'config-wrapper',
      ],
    ];
    $form['config_wrapper'] = [
      '#type' => 'container',
      // Force an id because otherwise default id is changed when using AJAX.
      '#attributes' => [
        'id' => 'config-wrapper',
      ],
    ];

    $remote_options = [];
    if ($default_remote = $channels_auto->get('remote')) {
      $remote_options = [
        $default_remote => $default_remote,
      ];
    }
    $form['config_wrapper']['remote'] = [
      '#type' => 'select',
      '#title' => 'Remote',
      '#validated' => TRUE,
      '#default_value' => $default_remote,
      '#options' => $remote_options,
      '#empty_value' => '',
      '#required' => TRUE,
    ];

    $import_config_options = [];
    if ($default_import_config = $channels_auto->get('import_config')) {
      $import_config_options = [
        $default_import_config => $default_import_config,
      ];
    }
    $form['config_wrapper']['import_config'] = [
      '#type' => 'select',
      '#title' => 'Import config',
      '#validated' => TRUE,
      '#default_value' => $default_import_config,
      '#options' => $import_config_options,
      '#empty_value' => '',
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $channels_auto = $this->entity;
    $status = $channels_auto->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Channels Auto.', [
          '%label' => $channels_auto->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Channels Auto.', [
          '%label' => $channels_auto->label(),
        ]));
    }
    $form_state->setRedirectUrl($channels_auto->toUrl('collection'));
  }

  /**
   * Get channel entities from entity share module.
   */
  protected function getChannelsEntityOptions() {
    $channel_entity = $this->entityTypeManager->getStorage('channel')
      ->loadMultiple();
    $options = [];
    foreach ($channel_entity as $channel_id => $channel) {
      $options[$channel_id] = $channel->label();
    }
    return $options;
  }

  /**
   * Ajax callback.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   Subform.
   */
  public static function buildAjaxRemoteSelect(array $form, FormStateInterface $form_state) {
    $remote_url = $form_state->getValue('remote_url');
    if (empty($remote_url)) {
      return $form['config_wrapper'];
    }
    $get_config_url = $remote_url . '/entity_share_auto_client/getRemoteAndImportConfig';
    $server_config = \Drupal::config('entity_share_auto.entity_share_auto_server.config');
    if ($api_key = $server_config->get('api_key')) {
      $get_config_url .= "?api_key=$api_key";
    }
    try {
      $request = \Drupal::httpClient()->request('GET', $get_config_url);
      if ($request->getStatusCode() != 200) {
        return $form['config_wrapper'];
      }

      $config = $request->getBody()->getContents();
      $config = json_decode($config, TRUE);
      $form['config_wrapper']['remote']['#options'] = $config['remote'];
      $form['config_wrapper']['import_config']['#options'] = $config['import_config'];
      return $form['config_wrapper'];
    }
    catch (\Exception $exception) {
      watchdog_exception('entity_share_auto_server', $exception);
      return $form['config_wrapper'];
    }
  }

}
