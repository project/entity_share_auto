<?php

namespace Drupal\entity_share_auto_server\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Channels Auto entities.
 */
interface ChannelsAutoInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
