<?php

namespace Drupal\entity_share_auto_server\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Channels Auto entity.
 *
 * @ConfigEntityType(
 *   id = "channels_auto",
 *   label = @Translation("Channels Auto"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\entity_share_auto_server\ChannelsAutoListBuilder",
 *     "form" = {
 *       "add" = "Drupal\entity_share_auto_server\Form\ChannelsAutoForm",
 *       "edit" = "Drupal\entity_share_auto_server\Form\ChannelsAutoForm",
 *       "delete" = "Drupal\entity_share_auto_server\Form\ChannelsAutoDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\entity_share_auto_server\ChannelsAutoHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "channels_auto",
 *   admin_permission = "administer channel auto config",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "channel_type" = "channel_type",
 *     "remote_url" = "remote_url",
 *     "remote" = "remote",
 *     "import_config" = "import_config"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/services/entity_share_auto/channels_auto/{channels_auto}",
 *     "add-form" = "/admin/config/services/entity_share_auto/channels_auto/add",
 *     "edit-form" = "/admin/config/services/entity_share_auto/channels_auto/{channels_auto}/edit",
 *     "delete-form" = "/admin/config/services/entity_share_auto/channels_auto/{channels_auto}/delete",
 *     "collection" = "/admin/config/services/entity_share_auto/channels_auto"
 *   }
 * )
 */
class ChannelsAuto extends ConfigEntityBase implements ChannelsAutoInterface {

  /**
   * The Channels Auto ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Channels Auto label.
   *
   * @var string
   */
  protected $label;

}
