<?php

namespace Drupal\entity_share_auto_server\Service;

use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\State\StateInterface;

/**
 * Populate a queue for server.
 *
 * @package Drupal\entity_share_auto_server\Service
 */
class EntityShareAutoExportQueueHelper implements EntityShareAutoQueueExportHelperInterface {

  /**
   * The queue factory service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * QueueHelper constructor.
   *
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(
    QueueFactory $queue_factory,
    StateInterface $state
  ) {
    $this->queueFactory = $queue_factory;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public function enqueue($remote_id, $channel_id, $import_config_id, $remote_url, $uuid) {
    $queue = $this->queueFactory->get(EntityShareAutoQueueExportHelperInterface::QUEUE_NAME);

    $export_states = $this->state->get(EntityShareAutoQueueExportHelperInterface::STATE_ID, []);
    $export_states_current = $export_states[$remote_id][$channel_id][$import_config_id] ?? [];
    if (empty($export_states) || !in_array($uuid, $export_states_current)) {
      // Add the entity to the export states.
      $export_states[$remote_id][$channel_id][$import_config_id][] = $uuid;

      // Create queue item.
      $item = [
        'uuid' => $uuid,
        'remote_id' => $remote_id,
        'channel_id' => $channel_id,
        'import_config_id' => $import_config_id,
        'remote_url' => $remote_url,
      ];

      $queue->createItem($item);
    }

    // Update states.
    $this->state->set(EntityShareAutoQueueExportHelperInterface::STATE_ID, $export_states);
  }

}
