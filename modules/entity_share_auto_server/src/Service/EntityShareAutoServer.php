<?php

namespace Drupal\entity_share_auto_server\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Drupal\entity_share\EntityShareUtility;
use Drupal\entity_share_auto\Event\EntityShareAutoEvent;
use Drupal\entity_share_auto\Exception\EntityShareAutoSkipExportException;
use Drupal\entity_share_server\Entity\ChannelInterface;
use Drupal\entity_share_server\Service\ChannelManipulatorInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class EntityShareAutoServer definition.
 */
class EntityShareAutoServer implements EntityShareAutoServerInterface {

  /**
   * EntityShareAutoQueueHelperInterface definition.
   *
   * @var \Drupal\entity_share_auto_server\Service\EntityShareAutoQueueExportHelperInterface
   */
  protected $entityShareAutoServerQueueHelper;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Guzzle\Client instance.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The channel manipulator.
   *
   * @var \Drupal\entity_share_server\Service\ChannelManipulatorInterface
   */
  protected $channelManipulator;

  /**
   * State service.
   *
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Client config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $serverConfig;

  /**
   * Constructs a new EntityShareAutoServer object.
   */
  public function __construct(EntityShareAutoQueueExportHelperInterface $entity_share_auto_server_queue_helper, EntityTypeManagerInterface $entity_type_manager, ClientInterface $http_client, LanguageManagerInterface $language_manager, ChannelManipulatorInterface $channel_manipulator, StateInterface $state, EventDispatcherInterface $event_dispatcher, ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory) {
    $this->entityShareAutoServerQueueHelper = $entity_share_auto_server_queue_helper;
    $this->entityTypeManager = $entity_type_manager;
    $this->httpClient = $http_client;
    $this->languageManager = $language_manager;
    $this->channelManipulator = $channel_manipulator;
    $this->state = $state;
    $this->eventDispatcher = $event_dispatcher;
    $this->moduleHandler = $module_handler;
    $this->serverConfig = $config_factory->get('entity_share_auto.entity_share_auto_server.config');
  }

  /**
   * Enqueue entity for export.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity for enqueue.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function enqueue(EntityInterface $entity) {
    $channels = $this->loadChannelWithBundle($entity);
    foreach ($channels as $channel) {
      try {
        $this->eventDispatcher->dispatch(new EntityShareAutoEvent($entity, $channel->get('channel_type')), EntityShareAutoEvent::ENTITY_SHARE_AUTO_ENTITY_ENQUEUE);
        // Check if instant export is enabled.
        if ($this->moduleHandler->moduleExists('entity_share_auto_instant') && $this->serverConfig->get('instant_export')) {
          /** @var \Drupal\entity_share_auto_instant\Service\EntityShareAutoInstantExport $instant_export_service */
          $instant_export_service = \Drupal::service('entity_share_auto_instant.export');
          $instant_export_service->exportEntity($channel, $entity);
        }
        else {
          $this->entityShareAutoServerQueueHelper->enqueue(
            $channel->get('remote'),
            $channel->get('channel_type'),
            $channel->get('import_config'),
            $channel->get('remote_url'),
            $entity->uuid()
                  );
        }
      }
      catch (EntityShareAutoSkipExportException $exception) {
        // Skip enqueue entity.
      }

    }
  }

  /**
   * Load channel entity according to auto share channel entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity object.
   *
   * @return array
   *   Return entity share auto channel config entity array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function loadChannelWithBundle(EntityInterface $entity) {
    $channels = $this->entityTypeManager->getStorage('channel')
      ->loadByProperties(['channel_bundle' => $entity->bundle()]);
    $channel_types = [];
    if (!empty($channels)) {
      foreach ($channels as $channel_id => $channel) {
        $content_on_channel = $this->checkEntitiesOnChannel($channel, $entity->uuid());
        if (!$content_on_channel) {
          continue;
        }
        $channel_auto_type = $this->entityTypeManager->getStorage('channels_auto')
          ->loadByProperties(['channel_type' => $channel_id]);
        if (!empty($channel_auto_type)) {
          foreach ($channel_auto_type as $channel_auto) {
            $channel_types[] = $channel_auto;
          }
        }
      }
    }
    return $channel_types;
  }

  /**
   * Helper function to check the presence of entities on a specific channel.
   *
   * @param \Drupal\entity_share_server\Entity\ChannelInterface $channel
   *   The channel id on which to check the entities.
   * @param string $entity_uuid
   *   The entity UUID to check for.
   */
  public function checkEntitiesOnChannel(ChannelInterface $channel, $entity_uuid) {
    $languages = $this->languageManager->getLanguages(LanguageInterface::STATE_ALL);
    $channel_entity_type = $channel->get('channel_entity_type');
    $channel_bundle = $channel->get('channel_bundle');
    $channel_langcode = $channel->get('channel_langcode');
    $route_name = sprintf('jsonapi.%s--%s.collection', $channel_entity_type, $channel_bundle);
    $url = Url::fromRoute($route_name)
      ->setOption('language', $languages[$channel_langcode])
      ->setOption('absolute', TRUE)
      ->setOption('query', $this->channelManipulator->getQuery($channel));

    // Prepare an URL to get only the UUIDs.
    $url_uuid = clone($url);
    $query = $url_uuid->getOption('query');
    $query = (!is_null($query)) ? $query : [];
    $url_uuid->setOption('query',
      $query + [
        'fields' => [
          $channel_entity_type . '--' . $channel_bundle => 'changed',
        ],
      ]
    );
    $prepared_url = EntityShareUtility::prepareUuidsFilteredUrl($url_uuid->toString(), [$entity_uuid]);
    $response = $this->httpClient->request('GET', $prepared_url, $this->getAuthenticationRequestOptions());
    $channel_url_response = Json::decode((string) $response->getBody());
    $found = FALSE;
    if (!empty($channel_url_response['data'])) {
      $found = TRUE;
    }
    return $found;
  }

  /**
   * Returns Guzzle request options for authentication.
   *
   * @return array
   *   Guzzle request options to use for authentication.
   *
   * @see \GuzzleHttp\ClientInterface::request()
   */
  protected function getAuthenticationRequestOptions() {
    if ($credentials = $this->state->get(EntityShareAutoServerInterface::BASIC_AUTH_KEY_VALUE_COLLECTION)) {
      return [
        RequestOptions::HEADERS => [
          'Content-Type' => 'application/vnd.api+json',
          'Authorization' => 'Basic ' . base64_encode($credentials['username'] . ':' . $credentials['password']),
        ],
      ];
    }
    else {
      return [
        RequestOptions::HEADERS => [
          'Content-Type' => 'application/vnd.api+json',
        ],
      ];
    }
  }

}
