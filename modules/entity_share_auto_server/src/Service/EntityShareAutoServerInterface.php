<?php

namespace Drupal\entity_share_auto_server\Service;

use Drupal\Core\Entity\EntityInterface;

/**
 * Interface EntityShareAutoServerInterface definition.
 */
interface EntityShareAutoServerInterface {

  /**
   * Basic auth key collection.
   */
  const BASIC_AUTH_KEY_VALUE_COLLECTION = 'entity_share_auto_server.jsonapi_authorization';

  /**
   * Enqueue node for export process.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity needs to export.
   *
   * @return mixed
   *   Return.
   */
  public function enqueue(EntityInterface $entity);

}
