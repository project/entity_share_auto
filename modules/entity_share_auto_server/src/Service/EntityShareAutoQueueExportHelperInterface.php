<?php

namespace Drupal\entity_share_auto_server\Service;

/**
 * Queue helper interface methods.
 */
interface EntityShareAutoQueueExportHelperInterface {

  /**
   * The queue ID.
   */
  const QUEUE_NAME = 'entity_share_auto_export';

  /**
   * The state ID.
   */
  const STATE_ID = 'entity_share_auto_export.states';

  /**
   * Enqueue entity to be synced later.
   *
   * @param string $remote_id
   *   The remote ID.
   * @param string $channel_id
   *   The channel ID.
   * @param string $import_config_id
   *   The import config ID.
   * @param string $remote_url
   *   The remote url of client.
   * @param string $uuid
   *   The UUIDs of the entities to pull.
   */
  public function enqueue($remote_id, $channel_id, $import_config_id, $remote_url, $uuid);

}
