<?php

namespace Drupal\entity_share_auto_server\Plugin\QueueWorker;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\State\StateInterface;
use Drupal\entity_share_auto_server\Service\EntityShareAutoQueueExportHelperInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the entity_share_auto_export queue worker.
 *
 * @QueueWorker (
 *   id = "entity_share_auto_export",
 *   title = @Translation("Entity share auto export queue.")
 * )
 */
class EntityShareAutoExportWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Guzzle\Client instance.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The state storage.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  private $stateStorage;


  /**
   * Client queue endpoint.
   */
  const CLIENT_ADD_QUEUE_URL = '/entity_share_auto_client/addImport';

  /**
   * Client config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $serverConfig;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    ClientInterface $http_client,
    LoggerChannelFactoryInterface $loggerFactory,
    StateInterface $state_storage,
    ConfigFactory $config_factory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->httpClient = $http_client;
    $this->loggerFactory = $loggerFactory;
    $this->stateStorage = $state_storage;
    $this->serverConfig = $config_factory->get('entity_share_auto.entity_share_auto_server.config');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('http_client'),
      $container->get('logger.factory'),
      $container->get('state'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $remote_base_url = $data['remote_url'];
    unset($data['remote_url']);
    $post_url = $remote_base_url . self::CLIENT_ADD_QUEUE_URL;
    if ($api_key = $this->serverConfig->get('api_key')) {
      $post_url .= "?api_key=$api_key";
    }

    // Remove entry from state.
    $async_states = $this->stateStorage->get(EntityShareAutoQueueExportHelperInterface::STATE_ID, []);
    $export_states_current = $async_states[$data['remote_id']][$data['channel_id']][$data['import_config_id']] ?? [];
    if (($key = array_search($data['uuid'], $export_states_current)) !== FALSE) {
      unset($async_states[$data['remote_id']][$data['channel_id']][$data['import_config_id']][$key]);
    }

    // Update states.
    $this->stateStorage->set(EntityShareAutoQueueExportHelperInterface::STATE_ID, $async_states);
    $response = $this->httpClient->post($post_url, ['json' => $data]);
    if ($response->getStatusCode() == 200) {
      $this->loggerFactory->get('entity_share_auto_server')->info("Item @uuid from channel @channel_id of remote @remote_id with the import config @import_config_id has been exported successfully.",
        [
          '@uuid' => $data['uuid'],
          '@channel_id' => $data['channel_id'],
          '@remote_id' => $data['remote_id'],
          '@import_config_id' => $data['import_config_id'],
        ]);
    }
    else {
      $this->loggerFactory->get('entity_share_auto_server')->error("Receive response error code @code from client, while syncing item @uuid from channel @channel_id of remote @remote_id with the import config @import_config_id.",
        [
          '@uuid' => $data['uuid'],
          '@channel_id' => $data['channel_id'],
          '@remote_id' => $data['remote_id'],
          '@import_config_id' => $data['import_config_id'],
          '@code' => $response->getStatusCode(),
        ]);
    }
  }

}
