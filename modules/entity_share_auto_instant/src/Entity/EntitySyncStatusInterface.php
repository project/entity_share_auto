<?php

namespace Drupal\entity_share_auto_instant\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Entity sync status entities.
 *
 * @ingroup entity_share_auto_instant
 */
interface EntitySyncStatusInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Entity sync status name.
   *
   * @return string
   *   Name of the Entity sync status.
   */
  public function getName();

  /**
   * Sets the Entity sync status name.
   *
   * @param string $name
   *   The Entity sync status name.
   *
   * @return \Drupal\entity_share_auto_instant\Entity\EntitySyncStatusInterface
   *   The called Entity sync status entity.
   */
  public function setName($name);

  /**
   * Gets the Entity sync status creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Entity sync status.
   */
  public function getCreatedTime();

  /**
   * Sets the Entity sync status creation timestamp.
   *
   * @param int $timestamp
   *   The Entity sync status creation timestamp.
   *
   * @return \Drupal\entity_share_auto_instant\Entity\EntitySyncStatusInterface
   *   The called Entity sync status entity.
   */
  public function setCreatedTime($timestamp);

}
