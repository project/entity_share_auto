<?php

namespace Drupal\entity_share_auto_instant\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Entity sync status entities.
 */
class EntitySyncStatusViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
