<?php

namespace Drupal\entity_share_auto_instant\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Entity sync status entity.
 *
 * @ingroup entity_share_auto_instant
 *
 * @ContentEntityType(
 *   id = "entity_sync_status",
 *   label = @Translation("Entity sync status"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\entity_share_auto_instant\EntitySyncStatusListBuilder",
 *     "views_data" = "Drupal\entity_share_auto_instant\Entity\EntitySyncStatusViewsData",
 *
 *     "access" = "Drupal\entity_share_auto_instant\EntitySyncStatusAccessControlHandler",
 *   },
 *   base_table = "entity_sync_status",
 *   translatable = FALSE,
 *   admin_permission = "administer entity sync status entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "entity_uuid",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 * )
 */
class EntitySyncStatus extends ContentEntityBase implements EntitySyncStatusInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  const STATUS_SUCCESS = 'success';
  const STATUS_FAILED = 'failed';
  const STATUS_IMPORTING = 'importing';
  const STATUS_UPDATING = 'updating';

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('uuid')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('uuid', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['entity_uuid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('UUID'))
      ->setDescription(t('UUID of entity synced.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['entity_label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity Label'))
      ->setDescription(t('Entity label.'));

    $fields['bundle'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity Type'))
      ->setDescription(t('Entity type of entity synced.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setRequired(TRUE);

    $fields['channel'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Channel'))
      ->setDescription(t('Channel name on entity synced.'))
      ->setRequired(TRUE);

    $fields['source_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Source ID'))
      ->setDescription(t('Source ID'))
      ->setRequired(TRUE);

    $fields['destination_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Destination ID'))
      ->setDescription(t('Destination ID'));

    $fields['response_message'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Response'))
      ->setDescription(t('Response message from client.'))
      ->setSetting('max_length', 255)
      ->setRequired(TRUE);

    $fields['sync_status'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Sync Status'))
      ->setDescription(t('A flag indicating whether this entity synced success or not.'))
      ->setRequired(TRUE)
      ->setSetting('allowed_values', [
        self::STATUS_SUCCESS => t('Success'),
        self::STATUS_FAILED => t('Failed'),
        self::STATUS_IMPORTING => t('Importing'),
        self::STATUS_UPDATING => t('Updating'),
      ]);

    $fields['status']->setDescription(t('A boolean indicating whether the Entity sync status is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
