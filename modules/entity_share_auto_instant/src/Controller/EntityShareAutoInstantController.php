<?php

namespace Drupal\entity_share_auto_instant\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EntityShareAutoInstantController definition.
 */
class EntityShareAutoInstantController extends ControllerBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Client config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $serverConfig;

  /**
   * Current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->serverConfig = $container->get('config.factory')
      ->get('entity_share_auto.entity_share_auto_server.config');
    $instance->request = $container->get('request_stack')->getCurrentRequest();
    $instance->logger = $container->get('logger.channel.entity_share_auto_instant');
    return $instance;
  }

  /**
   * Update log.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Return .
   */
  public function updateLog(Request $request) {
    try {
      // Check is server configured with API key validation or not.
      if ($api_key = $this->serverConfig->get('api_key')) {
        if ($this->request->query->get('api_key') != $api_key) {
          return new Response("Access denied.", 403);
        }
      }

      $post_data = json_decode($request->getContent(), TRUE);
      $data = $post_data['data'];
      if (!empty($data['uuid']) && $data['channel_id']) {
        $channel_auto_type = $this->entityTypeManager->getStorage('channels_auto')
          ->loadByProperties(['channel_type' => $data['channel_id']]);
        if (!empty($channel_auto_type)) {
          $channelsAuto = reset($channel_auto_type);
          $sync_entity_load = $this->entityTypeManager->getStorage('entity_sync_status')
            ->loadByProperties([
              'entity_uuid' => $data['uuid'],
              'channel' => $channelsAuto->get('label'),
            ]);
          if (!empty($sync_entity_load)) {
            $entity_sync = reset($sync_entity_load);
            if (isset($post_data['message']) &&  $post_data['message'] == 'Success' && !empty($post_data['ids'])) {
              $this->logger->info("Item @uuid from channel @channel_id of remote @remote_id with the import config @import_config_id has been exported successfully.",
                [
                  '@uuid' => $data['uuid'],
                  '@channel_id' => $data['channel_id'],
                  '@remote_id' => $data['remote_id'],
                  '@import_config_id' => $data['import_config_id'],
                ]);
              $entity_sync->set('response_message', $post_data['message']);
              $entity_sync->set('sync_status', 'success');
              $destination_id = $post_data['ids'][$data['uuid']] ?? '';
              $entity_sync->set('destination_id', $destination_id);
              $entity_sync->save();
              return new Response("Success", 200);
            }
            else {
              $this->logger->error("Receive response error message '@message' from client, while syncing item @uuid from channel @channel_id of remote @remote_id with the import config @import_config_id.",
                [
                  '@uuid' => $data['uuid'],
                  '@channel_id' => $data['channel_id'],
                  '@remote_id' => $data['remote_id'],
                  '@import_config_id' => $data['import_config_id'],
                  '@message' => $post_data['message'],
                ]);
              $entity_sync->set('response_message', $post_data['message']);
              $entity_sync->set('sync_status', 'failed');
              $destination_id = $post_data['ids'][$data['uuid']] ?? '';
              $entity_sync->set('destination_id', $destination_id);
              $entity_sync->save();
              return new Response("Success", 200);
            }
          }
        }
      }
    }
    catch (\Exception $exception) {
      watchdog_exception('entity_share_auto_client', $exception);
      return new Response('Not able to process you request at the moment. Please try again later.', 500);
    }
  }

}
