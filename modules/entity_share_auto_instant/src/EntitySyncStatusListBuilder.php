<?php

namespace Drupal\entity_share_auto_instant;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Entity sync status entities.
 *
 * @ingroup entity_share_auto_instant
 */
class EntitySyncStatusListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Entity sync status ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\entity_share_auto_instant\Entity\EntitySyncStatus $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.entity_sync_status.edit_form',
      ['entity_sync_status' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
