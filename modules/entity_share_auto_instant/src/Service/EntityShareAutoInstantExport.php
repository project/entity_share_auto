<?php

namespace Drupal\entity_share_auto_instant\Service;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_share_auto_server\Entity\ChannelsAutoInterface;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;

/**
 * Class EntityShareAutoInstantExport definition.
 */
class EntityShareAutoInstantExport {

  /**
   * Client queue endpoint.
   */
  const CLIENT_INSTANT_EXPORT = '/entity_share_auto_client/import_instant';


  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Client config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $serverConfig;

  /**
   * Logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new EntityShareAutoInstantExport object.
   */
  public function __construct(ClientInterface $http_client, ConfigFactory $config_factory, LoggerInterface $logger, EntityTypeManagerInterface $entity_type_manager) {
    $this->httpClient = $http_client;
    $this->serverConfig = $config_factory->get('entity_share_auto.entity_share_auto_server.config');
    $this->logger = $logger;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Export entity.
   *
   * @param \Drupal\entity_share_auto_server\Entity\ChannelsAutoInterface $channelsAuto
   *   Channel auto config entity.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Export entity.
   */
  public function exportEntity(ChannelsAutoInterface $channelsAuto, EntityInterface $entity) {
    $data = [
      'remote_id' => $channelsAuto->get('remote'),
      'channel_id' => $channelsAuto->get('channel_type'),
      'import_config_id' => $channelsAuto->get('import_config'),
      'uuid' => $entity->uuid(),
    ];
    $post_url = $channelsAuto->get('remote_url') . self::CLIENT_INSTANT_EXPORT;
    if ($api_key = $this->serverConfig->get('api_key')) {
      $post_url .= "?api_key=$api_key";
    }
    try {
      // Create log entry with status importing.
      $this->createStubSyncEntityLog($channelsAuto, $entity, 'importing');
      // Timeout for 3 minutes for large data.
      $response = $this->httpClient->post($post_url, [
        'timeout' => 180,
        'json' => $data,
      ]);
      if ($response->getStatusCode() == 200) {
        $this->logger->info("Item @uuid from channel @channel_id of remote @remote_id with the import config @import_config_id will start import shortly at client.",
          [
            '@uuid' => $data['uuid'],
            '@channel_id' => $data['channel_id'],
            '@remote_id' => $data['remote_id'],
            '@import_config_id' => $data['import_config_id'],
          ]);
      }
      else {
        $this->logger->error("Receive response error code @code from client, while syncing item @uuid from channel @channel_id of remote @remote_id with the import config @import_config_id.",
          [
            '@uuid' => $data['uuid'],
            '@channel_id' => $data['channel_id'],
            '@remote_id' => $data['remote_id'],
            '@import_config_id' => $data['import_config_id'],
            '@code' => $response->getStatusCode(),
          ]);
        $this->setFailedStatus($channelsAuto, $data);
      }
    }
    catch (\Exception $exception) {
      $this->setFailedStatus($channelsAuto, $data);
      watchdog_exception('entity_share_auto_instant', $exception);
    }
  }

  /**
   * Create/update stub log entry. Using custom content entity.
   *
   * @param \Drupal\entity_share_auto_server\Entity\ChannelsAutoInterface $channelsAuto
   *   Channel auto entity.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity object.
   * @param string $status
   *   Status to be updated.
   */
  protected function createStubSyncEntityLog(ChannelsAutoInterface $channelsAuto, EntityInterface $entity, $status) {
    $sync_entity = [
      'entity_uuid' => $entity->uuid(),
      'entity_label' => $entity->label(),
      'bundle' => $entity->bundle(),
      'channel' => $channelsAuto->get('label'),
      'source_id' => $entity->id(),
      'sync_status' => $status,
    ];

    try {
      $sync_entity_load = $this->entityTypeManager->getStorage('entity_sync_status')
        ->loadByProperties([
          'entity_uuid' => $entity->uuid(),
          'channel' => $channelsAuto->get('label'),
        ]);
      if (!empty($sync_entity_load)) {
        $entity_sync = reset($sync_entity_load);
        // Reset response message.
        if ($entity_sync) {
          $entity_sync->set('entity_label', $entity->label());
          $entity_sync->set('response_message', '');
          $entity_sync->set('sync_status', 'updating');
          $entity_sync->save();
        }
      }
      else {
        $this->entityTypeManager->getStorage('entity_sync_status')->create($sync_entity)->save();
      }
    }
    catch (\Exception $exception) {
      watchdog_exception('entity_share_auto_instant', $exception);
    }
  }

  /**
   * Set failed status for sync entity.
   *
   * @param \Drupal\entity_share_auto_server\Entity\ChannelsAutoInterface $channelsAuto
   *   Channel auto.
   * @param array $data
   *   Item data.
   */
  public function setFailedStatus(ChannelsAutoInterface $channelsAuto, array $data) {
    $sync_entity_load = $this->entityTypeManager->getStorage('entity_sync_status')
      ->loadByProperties([
        'entity_uuid' => $data['uuid'],
        'channel' => $channelsAuto->get('label'),
      ]);
    if (!empty($sync_entity_load)) {
      $entity_sync = reset($sync_entity_load);
      if ($entity_sync) {
        $entity_sync->set('response_message', $data['message']);
        $entity_sync->set('sync_status', 'failed');
        $entity_sync->save();
      }
    }
  }

}
