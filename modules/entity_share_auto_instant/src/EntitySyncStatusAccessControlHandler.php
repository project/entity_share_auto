<?php

namespace Drupal\entity_share_auto_instant;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Entity sync status entity.
 *
 * @see \Drupal\entity_share_auto_instant\Entity\EntitySyncStatus.
 */
class EntitySyncStatusAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\entity_share_auto_instant\Entity\EntitySyncStatusInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished entity sync status entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published entity sync status entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit entity sync status entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete entity sync status entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add entity sync status entities');
  }

}
